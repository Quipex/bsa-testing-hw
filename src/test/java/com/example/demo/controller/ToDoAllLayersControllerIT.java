package com.example.demo.controller;

import com.example.demo.model.ToDoEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ToDoAllLayersControllerIT {
	
	@Autowired
	private MockMvc mvc;
	
	private ObjectMapper objectMapper = new ObjectMapper();
	
	@Test // new
	void whenSaveTodo_thenValidResponse() throws Exception {
		var newTodo = newTodo();
		var createResp = saveTodoAndVerify(newTodo);
		
		long createdId = Long.parseLong(extractByJsonPath(createResp, "$.id"));
		deleteTodoAndVerify(createdId);
	}
	
	@Test // new
	void whenUpdateTodo_thenValidResponse() throws Exception {
//		saving new
		var newTodo = newTodo();
		var createResp = saveTodoAndVerify(newTodo);
		
//		updating recently saved
		long createdId = Long.parseLong(extractByJsonPath(createResp, "$.id"));
		String updatedText = "Updated Todo";
		var updatedTodo = new ToDoEntity(createdId, updatedText);
		var updateResp = saveTodoAndVerify(updatedTodo);
		
//		asserting
		long updatedId = Long.parseLong(extractByJsonPath(updateResp, "$.id"));
		assertEquals(createdId, updatedId);
		assertEquals(updatedText, extractByJsonPath(updateResp, "$.text"));
		
		deleteTodoAndVerify(updatedId);
	}
	
	@Test // new
	void whenCompletingNoneExistingTodo_thenNotFound() throws Exception {
		mvc.perform(put("/todos/{id}/complete", -1L))
				.andExpect(status().isNotFound());
		
	}
	
	private String extractByJsonPath(MvcResult createResp, String jsonPath) throws UnsupportedEncodingException {
		return JsonPath.read(createResp.getResponse().getContentAsString(), jsonPath).toString();
	}
	
	private void deleteTodoAndVerify(long createdId) throws Exception {
		mvc.perform(delete("/todos/" + createdId))
				.andExpect(status().isOk());
	}
	
	private ToDoEntity newTodo() {
		return new ToDoEntity("New todo. createdAt=" + LocalDateTime.now());
		
	}
	
	private MvcResult saveTodoAndVerify(ToDoEntity newTodo) throws Exception {
		return mvc.perform(post("/todos").content(asJson(newTodo)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").exists())
				.andExpect(jsonPath("$.id").isNumber())
				.andExpect(jsonPath("$.text").value(newTodo.getText()))
				.andExpect(jsonPath("$.completedAt").doesNotExist())
				.andReturn();
	}
	
	private String asJson(Object object) {
		try {
			return objectMapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}
}
