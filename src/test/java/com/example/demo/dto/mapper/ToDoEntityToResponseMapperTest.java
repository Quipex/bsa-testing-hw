package com.example.demo.dto.mapper;

import com.example.demo.model.ToDoEntity;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class ToDoEntityToResponseMapperTest {
	
	@Test // new
	void givenNullObject_returnsNull() {
		assertNull(ToDoEntityToResponseMapper.map(null));
	}
	
	@Test // new
	void givenValidTodo_returnsSamePropertiesDto() {
		var todo = new ToDoEntity(0L, "First");
		todo.completeNow();
		var mapped = ToDoEntityToResponseMapper.map(todo);
		assertEquals(todo.getId(), mapped.id);
		assertEquals(todo.getText(), mapped.text);
		assertEquals(todo.getCompletedAt(), mapped.completedAt);
	}

}
